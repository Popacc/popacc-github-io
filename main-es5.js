(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" fxLayout=\"column\">\r\n  <app-header></app-header>\r\n  <app-first-page></app-first-page>\r\n  <app-second-page></app-second-page>\r\n  <app-third-page></app-third-page>\r\n  <app-fourth-page></app-fourth-page>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/app-description/app-description.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/app-description/app-description.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"column\" fxLayoutGap=\"48px\">\r\n  <div\r\n    fxLayout=\"row\"\r\n    class=\"app-questions\"\r\n    fxLayoutAling=\"space-evenly center\"\r\n    fxLayout.lt-md=\"column\"\r\n  >\r\n    <div class=\"text1\">\r\n      <span class=\"mat-display-4\">Šta</span> <br />je moj trening?\r\n    </div>\r\n    <div class=\"text1\">\r\n      <span class=\"mat-display-4\">Kako</span> <br />može da mi pomogne?\r\n    </div>\r\n  </div>\r\n  <div class=\"container\">\r\n    <div\r\n      fxLayout=\"row\"\r\n      fxLayoutGap=\"24px\"\r\n      class=\"app-answers\"\r\n      fxLayout.lt-md=\"column\"\r\n    >\r\n      <div fxLayout=\"column\" class=\"item\" fxLayoutAlign=\"center center\">\r\n        <img src=\"../../../assets/images/bolt.png\" alt=\"Munja\" />\r\n        <div fxLayout=\"column\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            TektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            ktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            TektsTektsTektsTektsTektsTektsTekts\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"column\" class=\"item\" fxLayoutAlign=\"center center\">\r\n        <img src=\"../../../assets/images/bolt.png\" alt=\"Munja\" />\r\n        <div fxLayout=\"column\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            TektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            ktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            TektsTektsTektsTektsTektsTektsTekts\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"column\" class=\"item\" fxLayoutAlign=\"center center\">\r\n        <img src=\"../../../assets/images/bolt.png\" alt=\"Munja\" />\r\n        <div fxLayout=\"column\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            TektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            ktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            TektsTektsTektsTektsTektsTektsTekts\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"column\" class=\"item\" fxLayoutAlign=\"center center\">\r\n        <img src=\"../../../assets/images/bolt.png\" alt=\"Munja\" />\r\n        <div fxLayout=\"column\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            TektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            ktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            TektsTektsTektsTektsTektsTektsTekts\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"column\" class=\"item\" fxLayoutAlign=\"center center\">\r\n        <img src=\"../../../assets/images/bolt.png\" alt=\"Munja\" />\r\n        <div fxLayout=\"column\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            TektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            ktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            TektsTektsTektsTektsTektsTektsTekts\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/first-page/first-page.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/first-page/first-page.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div\n  fxLayout=\"column\"\n  class=\"first-page page\"\n>\n  <div fxLayoutAlign=\"flex-start center\">\n    <div class=\"mat-display-3\" fxFlex=\"45%\">\n      Kako od jednog do <b>2000</b> klijenata?\n    </div>\n  </div>\n\n  <div\n    fxLayout=\"row\"\n    fxLayoutAlign=\"center start\"\n    fxLayoutAlign.lt-lg=\"center\"\n    fxLayoutGap=\"48px\"\n    fxLayout.lt-lg=\"column\"\n  >\n    <div\n      fxLayout=\"column\"\n      fxFlex=\"40%\"\n      fxLayoutAlign=\"center center\"\n      fxLayoutGap=\"48px\"\n      fxLayoutGap.lt-lg=\"6px\"\n    >\n      <iframe [src]=\"safeURL\" frameborder=\"0\" allowfullscreen></iframe>\n    </div>\n    <div fxLayout=\"column\" fxFlex=\"60%\" fxLayoutGap=\"12px\">\n      <div fxLayout=\"row\" class=\"mat-display-1 text-list\">\n        <mat-icon matListIcon class=\"mat-display-2\">check_box</mat-icon>\n        Da li još uvek misliš da je nemoguće trenirati više od 10 klijenata\n        dnevno?\n      </div>\n      <div fxLayout=\"row\" class=\"mat-display-1 text-list\">\n        <mat-icon matListIcon class=\"mat-display-2\">check_box</mat-icon>\n        Da broj tvojih radnih sati definiše tvoje prihode? <br />Da se posao\n        trenera isplati samo u inostranstvu?\n      </div>\n      <div fxLayout=\"row\" class=\"mat-display-1 text-list\">\n        <mat-icon matListIcon class=\"mat-display-2\">check_box</mat-icon>\n        NE MORA VIŠE TAKO!<br />\n        NE MORA NAPORNIJE, MOŽE PAMETNIJE.\n      </div>\n      <div fxLayout=\"row\" class=\"mat-display-1 text-list\">\n        <mat-icon matListIcon class=\"mat-display-2\">check_box</mat-icon>\n        Moj-trening je aplikacija koja ti omogućava postavljanje treninga i\n        ishrane u samo 2 koraka i sada je možeš isprobati potpuno BESPLATNO 30\n        dana!\n      </div>\n    </div>\n  </div>\n  <div\n    fxLayout=\"row\"\n    fxLayout.lt-lg=\"column\"\n    fxLayoutAlign=\"flex-start start\"\n    fxLayoutGap=\"48px\"\n    class=\"form\"\n  >\n    <div class=\"mat-display-1 warn\" fxFlex=\"40%\" fxLayout=\"column\">\n      POŽURITE! <br />PONUDA ISTIČE USKORO\n      <countdown\n        [config]=\"{ leftTime: 360 * 60 }\"\n        class=\"mat-display-3\"\n        >$!h!h:$!m!m:$!s!s</countdown\n      >\n    </div>\n    <form\n      fxLayout=\"column\"\n      fxFlex=\"60%\"\n      fxLayoutGap=\"12px\"\n      fxFlex.lt-lg=\"100%\"\n      fxLayoutAlign.lt-lg=\"center center\"\n    >\n      <mat-form-field floatLabel=\"never\">\n        <input matInput placeholder=\"Unesite svoje ime...\" />\n      </mat-form-field>\n      <mat-form-field floatLabel=\"never\">\n        <input matInput placeholder=\"Unesite svoj mejl...\" />\n      </mat-form-field>\n      <button mat-flat-button class=\"pulse-button\">\n        Kreirajte nalog\n      </button>\n    </form>\n  </div>\n</div> -->\n<div\n  fxLayout=\"column\"\n  class=\"first-page page\"\n  fxLayoutGap=\"48px\"\n  fxLayoutAlign=\"center center\"\n>\n  <div class=\"page-title\">\n    Onlajn personal treneri paznja <br />\n    Jedini softver koji ce vam ikada trebati\n  </div>\n  <div class=\"text-below-title\">\n    Zaboravite viber, email, whatsapp, google doc, pdf, youtube, ... sada vam je\n    sve na jednom mestu. Ustedite vreme i impresioniraj svoje klijente\n  </div>\n  <iframe [src]=\"safeURL\" frameborder=\"0\" allowfullscreen></iframe>\n  <form fxLayout=\"column\" fxLayoutGap=\"12px\" fxLayoutAlign=\"center center\">\n    <mat-form-field floatLabel=\"never\" fxLayoutAlign=\"center center\">\n      <input matInput placeholder=\"Unesite svoje ime...\" />\n    </mat-form-field>\n    <mat-form-field floatLabel=\"never\" fxLayoutAlign=\"center center\">\n      <input matInput placeholder=\"Unesite svoj mejl...\" />\n    </mat-form-field>\n    <button mat-flat-button class=\"pulse-button\">\n      Probaj besplatno 30 dana\n    </button>\n  </form>\n  <div class=\"text-below-form\" fxLayout=\"column\" fxLayoutGap=\"16px\">\n    <div fxLayout=\"row\" fxLayoutGap=\"6px\" fxLayoutAlign=\"start center\">\n      <mat-icon matListIcon>check_box</mat-icon>\n      <div>Otvaranje naloga klijentima i postavljanje plana treninga i ishrane ispod 30 sekundi</div>\n    </div>\n    <div fxLayout=\"row\"  fxLayoutGap=\"6px\"  fxLayoutAlign=\"start center\">\n      <mat-icon matListIcon>check_box</mat-icon>\n      <div>Superiorno pravljenje planova treninga i ishrane</div>\n    </div>\n    <div fxLayout=\"row\"  fxLayoutGap=\"6px\"  fxLayoutAlign=\"start center\">\n      <mat-icon matListIcon>check_box</mat-icon>\n      <div>Biblioteke od preko 1000 vezbi i treninga</div>\n    </div>\n    <div fxLayout=\"row\"  fxLayoutGap=\"6px\"  fxLayoutAlign=\"start center\">\n      <mat-icon matListIcon>check_box</mat-icon>\n      <div>Filtriranje preko 1000 recepata po odnosu makronutrijenata</div>\n    </div>\n    <div fxLayout=\"row\"  fxLayoutGap=\"6px\"  fxLayoutAlign=\"start center\">\n      <mat-icon matListIcon>check_box</mat-icon>\n      <div>Pracenje programa, cet, automatsko slanje poruka, pracenje isteka planova, ...</div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/fourth-page/fourth-page.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/fourth-page/fourth-page.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"fourth-page page\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"12px\">\n    <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\n            <img src=\"../../../assets/images/strongman-thinking.png\" fxFlex=\"50%\" />\n            <div class=\"text-title\" fxFlex=\"50%\">\n                Sa onlajnom je ponovo postalo tesko\n            </div>\n        </div>\n        <div class=\"text-below-title\">\n          Nikad nisam zamišljao sebe kao nekog ko radi od 7 do 15. Od toga me podilazi\n          jeza niz kičmu. Mislim da ja mogu bolje od toga. Mislim da mogu bolje od par\n          stotina eura, toplog obroka i dodatka za javni prevoz. Sebe sam zamišljao\n          kao nekog ko živi od svog znanja, a ljudi mu se zahvaljuju na tome, po\n          mogućstvu u parama. U stotinama eura. Zanemarivši sva svoja druga zanimanja\n          odlučio sam da budem samo personalni trener i to puno radno vreme.\n        </div>\n      </div>\n      "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/header/header.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/header/header.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n  <div class=\"header-logo-container\">\r\n    <img\r\n      src=\"../../../assets/images/logo.svg\"\r\n      class=\"header-logo\"\r\n      alt=\"Logotip\"\r\n    />\r\n  </div>\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <button mat-raised-button>Uloguj se</button>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/new-user-snackbar/new-user-snackbar.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/new-user-snackbar/new-user-snackbar.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutGap=\"12px\" >\n <img src=\"../../../assets/images/strongmen.jpg\" alt=\"Slika\"/>\n  <div fxLayout=\"column\" fxLayoutAlign=\"center\">\n    <span >Petar, Beograd</span>\n    <span>Upravo kreirao/la nalog</span>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/prices/prices.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/prices/prices.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"column\" fxLayoutGap=\"48px\">\r\n  <div class=\"price-title\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <div>\r\n      <span class=\"mat-display-4\">Cene</span><br />ako uštediš makar sat vremena\r\n      nedeljno ili imaš novog klijenta - vredi\r\n    </div>\r\n  </div>\r\n  <div\r\n    class=\"container\"\r\n    fxLayout=\"row\"\r\n    fxLayout.lt-lg=\"column\"\r\n    fxLayoutGap.lt-lg=\"24px\"\r\n    fxLayoutAlign=\"space-evenly center\"\r\n  >\r\n    <div fxLayout=\"column\" class=\"price-item\">\r\n      <span class=\"mat-h1\">Početnik</span>\r\n      <div>\r\n        <span>20&euro;</span>\r\n        <br />\r\n        do 5 klijenata\r\n      </div>\r\n      <mat-divider></mat-divider>\r\n      <div>\r\n        <span>50&euro;</span>\r\n\r\n        <br />\r\n        do 20 klijenata\r\n      </div>\r\n    </div>\r\n    <div fxLayout=\"column\" class=\"price-item\">\r\n      <span class=\"mat-h1\">Profesionalac</span>\r\n      <div>\r\n        <span>70&euro;</span>\r\n\r\n        <br />\r\n        do 30 klijenata\r\n      </div>\r\n      <mat-divider></mat-divider>\r\n      <div>\r\n        <span>100&euro;</span>\r\n        <br />\r\n        do 50 klijenata\r\n      </div>\r\n    </div>\r\n    <div fxLayout=\"column\" class=\"price-item\">\r\n      <span class=\"mat-h1\">Institucija</span>\r\n      <div>\r\n        <span>160&euro;</span>\r\n\r\n        <br />\r\n        do 100 klijenata\r\n      </div>\r\n      <mat-divider></mat-divider>\r\n      <div>\r\n        <span>200&euro;</span>\r\n\r\n        <br />\r\n        do 200 klijenata <br />\r\n        <span class=\"euro-saver\">\r\n          preko 200 je 1 &euro; po klijentu\r\n        </span>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/second-page/second-page.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/second-page/second-page.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"page\">\n    <div class=\"second-page-image-wrapper\"></div>\n    <div fxLayoutAlign=\"flex-end\" class=\"second-page-text\">\n      <div fxFlex=\"55%\" fxLayout=\"column\" fxFlex.lt-lg=\"100%\">\n        <div class=\"mat-display-3\">Namučio sam se u početku</div>\n        <div class=\"mat-display-1\">\n          <p>\n            Da vas zainteresujem... Ja sam onaj bajča što ima par hiljada onlajn\n            klijenata, a do pre par godina sam bio drvo, ali baš apsolutno drvo za\n            onlajn biznis. U početku sam lomio glavu kako da nađem klijente, a\n            posle mnogo objavljenih before after fotografija mojih klijenata i\n            edukativnih postova kad je krenulo nisam mogao da pohvatam kome pišem\n            na viber, kome šaljem pdf na email a kome u mesindžeru video sa\n            tehnikom izvođenja.\n          </p>\n          <p>\n            Da budem iskren nisam imao jasan plan. Znao sam samo da moram da radim\n            i da se trudim. Jedna stvar je povukla drugu, pa je došla treća... I\n            sad sam Kardašijan onlajn biznisa za trenere. Ne mislim na veličinu\n            zadnjice. Iako imam najveći stomak (nikad pločice) imam impozantan\n            broj klijenata. Kako?\n          </p>\n        </div>\n        <div fxLayout=\"row\" fxLayoutAlign=\"center\" fxLayoutAlign=\"center center\" fxFlex=\"100%\">\n          <button mat-flat-button>Nastavi sa čitanjem</button>\n        </div>\n      </div>\n    </div>\n  </div> -->\n<div class=\"second-page page\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"12px\">\n  <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\n    <img src=\"../../../assets/images/strongman-surprised\" fxFlex=\"50%\" />\n    <div class=\"text-title\" fxFlex=\"50%\">\n      Pakao kroz koji svi prolazimo u pocetku\n    </div>\n  </div>\n  <div class=\"text-below-title\">\n    Nikad nisam zamišljao sebe kao nekog ko radi od 7 do 15. Od toga me podilazi\n    jeza niz kičmu. Mislim da ja mogu bolje od toga. Mislim da mogu bolje od par\n    stotina eura, toplog obroka i dodatka za javni prevoz. Sebe sam zamišljao\n    kao nekog ko živi od svog znanja, a ljudi mu se zahvaljuju na tome, po\n    mogućstvu u parama. U stotinama eura. Zanemarivši sva svoja druga zanimanja\n    odlučio sam da budem samo personalni trener i to puno radno vreme.\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/strongman/strongman.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/strongman/strongman.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"column\" fxLayoutGap=\"48px\">\r\n  <div class=\"strongman-question\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <div class=\"mat-display-4\">Zašto moj-trening?</div>\r\n  </div>\r\n  <div class=\"container\">\r\n    <div\r\n      fxLayout=\"row\"\r\n      fxLayout.lt-lg=\"column\"\r\n      fxLayoutAlign.lt-lg=\"center center\"\r\n    >\r\n      <div fxFlex=\"30%\" class=\"image\">\r\n        <img src=\"../../../assets/images/strongmen.jpg\" alt=\"Slika\" />\r\n      </div>\r\n      <div fxFlex=\"70%\">\r\n        <span class=\"mat-display-2\"\r\n          >Milan strongman, prvi onlajn trener u srbiji</span\r\n        ><br />\r\n        <div class=\"mat-h1\" class=\"text\">\r\n          <!-- Kao onlajn trener imao sam preko 8000 klijenata i probao sam sve\r\n          četiri aplikacije koje postoje na tržištu. I kod svake sam imao neki\r\n          problem a i mrzeo sam da nosim uvek lap top sa sobom. Skupili smo tim\r\n          i za 2 godine napravili softver od trenera za trenera. Isprobajte\r\n          besplatno 30 dana. Otvoreni smo za predloge i sugestije. Još samo par\r\n          dana poklanjam moju e-knjigu\r\n          <b>\"Kako da nađete svoje prve klijente\"</b>. Prijavi se i poslaću ti\r\n          knjigu na messenger -->\r\n          {{ text }}\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/third-page/third-page.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/third-page/third-page.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"third-page page\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"12px\">\n    <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\n        <div class=\"text-title\" fxFlex=\"50%\">\n            Kako sam postao \"Onlajn trener\"\n        </div>\n        <img src=\"../../../assets/images/strongman-surprised\" fxFlex=\"50%\" />\n    </div>\n    <div class=\"text-below-title\">\n      Nikad nisam zamišljao sebe kao nekog ko radi od 7 do 15. Od toga me podilazi\n      jeza niz kičmu. Mislim da ja mogu bolje od toga. Mislim da mogu bolje od par\n      stotina eura, toplog obroka i dodatka za javni prevoz. Sebe sam zamišljao\n      kao nekog ko živi od svog znanja, a ljudi mu se zahvaljuju na tome, po\n      mogućstvu u parama. U stotinama eura. Zanemarivši sva svoja druga zanimanja\n      odlučio sam da budem samo personalni trener i to puno radno vreme.\n    </div>\n  </div>\n  "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/trainers/trainers.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/trainers/trainers.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"column\" fxLayoutGap=\"48px\">\r\n  <div class=\"trainer-question\" fxLayout=\"row\">\r\n    <div>\r\n      <span class=\"mat-display-4\">Sta kažu treneri</span> <br />o saradnji sa\r\n      nama?\r\n    </div>\r\n  </div>\r\n  <div\r\n    class=\"container\"\r\n    fxLayout=\"row\"\r\n    fxLayoutGap=\"24px\"\r\n    fxLayout.lt-lg=\"column\"\r\n    fxLayoutAlign=\"space-between flex-start\"\r\n  >\r\n    <div fxLayout=\"column\" fxLayoutGap=\"12px\" fxFlex=\"50%\">\r\n      <img\r\n        src=\"../../../assets/images/app-sample.jpg\"\r\n        alt=\"Image\"\r\n        class=\"app-image\"\r\n      />\r\n      <form\r\n        fxLayoutAlign=\"center first-baseline\"\r\n        fxLayout.lt-lg=\"column\"\r\n        fxLayoutGap=\"12px\"\r\n        fxLayout=\"row\"\r\n      >\r\n        <mat-form-field fxFlex=\"70%\" appearance=\"outline\">\r\n          <input matInput placeholder=\"Unesite svoj mejl...\" />\r\n          <mat-hint>Zapocnite besplatno 30 dana*</mat-hint>\r\n        </mat-form-field>\r\n        <button mat-flat-button fxFlex=\"30%\">\r\n          Kreirajte nalog\r\n        </button>\r\n      </form>\r\n    </div>\r\n    <div fxLayout=\"column\" fxFlex=\"50%\" fxLayoutGap=\"24px\" class=\"trainers\">\r\n      <div fxLayout=\"row\">\r\n        <img\r\n          src=\"../../../assets/images/hanibal.jpg\"\r\n          alt=\"Slika\"\r\n          class=\"trainer-image\"\r\n        />\r\n        <div fxLayout=\"column\" class=\"text-wrapper\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            Proba neka. <br />\r\n            Dobar trening sve pohvale. <br />Mnogo dobro kad pijes protein.\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"row\">\r\n        <img\r\n          src=\"../../../assets/images/hanibal.jpg\"\r\n          alt=\"Slika\"\r\n          class=\"trainer-image\"\r\n        />\r\n        <div fxLayout=\"column\" class=\"text-wrapper\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            TektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            ktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            TektsTektsTektsTektsTektsTektsTekts\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"row\">\r\n        <img\r\n          src=\"../../../assets/images/hanibal.jpg\"\r\n          alt=\"Slika\"\r\n          class=\"trainer-image\"\r\n        />\r\n        <div fxLayout=\"column\" class=\"text-wrapper\">\r\n          <div class=\"text mat-h1\">Brzo i efikasno</div>\r\n          <div class=\"text\">\r\n            TektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            ktsTektsTektsTektsTektsTektsTektsTektsTektsTektsTektsTekts\r\n            TektsTektsTektsTektsTektsTektsTekts\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  width: 100%;\n  height: 100%;\n  margin: 0;\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXFVzZXJzXFxOZW1hbmphXFxEb2N1bWVudHNcXEdpdCBQcm9qZWN0c1xcYW5ndWxhci1wcm9qZWN0c1xcdHJhaW4tbWUtb25saW5lL3NyY1xcYXBwXFxhcHAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZzogMDtcclxufVxyXG5cclxuIiwiLmNvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'train-me-online';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _pages_header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/header/header.component */ "./src/app/pages/header/header.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _modules_material_material_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/material/material.module */ "./src/app/modules/material/material.module.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _pages_app_description_app_description_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/app-description/app-description.component */ "./src/app/pages/app-description/app-description.component.ts");
/* harmony import */ var _pages_trainers_trainers_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/trainers/trainers.component */ "./src/app/pages/trainers/trainers.component.ts");
/* harmony import */ var _pages_strongman_strongman_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/strongman/strongman.component */ "./src/app/pages/strongman/strongman.component.ts");
/* harmony import */ var _pages_prices_prices_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/prices/prices.component */ "./src/app/pages/prices/prices.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/fesm5/ngx-countdown.js");
/* harmony import */ var _pages_first_page_first_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/first-page/first-page.component */ "./src/app/pages/first-page/first-page.component.ts");
/* harmony import */ var _pages_second_page_second_page_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/second-page/second-page.component */ "./src/app/pages/second-page/second-page.component.ts");
/* harmony import */ var _pages_new_user_snackbar_new_user_snackbar_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/new-user-snackbar/new-user-snackbar.component */ "./src/app/pages/new-user-snackbar/new-user-snackbar.component.ts");
/* harmony import */ var _pages_third_page_third_page_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pages/third-page/third-page.component */ "./src/app/pages/third-page/third-page.component.ts");
/* harmony import */ var _pages_fourth_page_fourth_page_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pages/fourth-page/fourth-page.component */ "./src/app/pages/fourth-page/fourth-page.component.ts");




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _pages_header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"],
                _pages_app_description_app_description_component__WEBPACK_IMPORTED_MODULE_9__["AppDescriptionComponent"],
                _pages_trainers_trainers_component__WEBPACK_IMPORTED_MODULE_10__["TrainersComponent"],
                _pages_strongman_strongman_component__WEBPACK_IMPORTED_MODULE_11__["StrongmanComponent"],
                _pages_prices_prices_component__WEBPACK_IMPORTED_MODULE_12__["PricesComponent"],
                _pages_first_page_first_page_component__WEBPACK_IMPORTED_MODULE_15__["FirstPageComponent"],
                _pages_second_page_second_page_component__WEBPACK_IMPORTED_MODULE_16__["SecondPageComponent"],
                _pages_new_user_snackbar_new_user_snackbar_component__WEBPACK_IMPORTED_MODULE_17__["NewUserSnackbarComponent"],
                _pages_third_page_third_page_component__WEBPACK_IMPORTED_MODULE_18__["ThirdPageComponent"],
                _pages_fourth_page_fourth_page_component__WEBPACK_IMPORTED_MODULE_19__["FourthPageComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                _modules_material_material_module__WEBPACK_IMPORTED_MODULE_7__["MaterialModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_8__["FlexLayoutModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                ngx_countdown__WEBPACK_IMPORTED_MODULE_14__["CountdownModule"]
            ],
            entryComponents: [_pages_new_user_snackbar_new_user_snackbar_component__WEBPACK_IMPORTED_MODULE_17__["NewUserSnackbarComponent"]],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/modules/material/material.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/modules/material/material.module.ts ***!
  \*****************************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/ */ "./node_modules/@angular/material/esm5/material.es5.js");



var MaterialComponents = [
    _angular_material___WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
    _angular_material___WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
    _angular_material___WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
    _angular_material___WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
    _angular_material___WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"],
    _angular_material___WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"]
];
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [MaterialComponents],
            exports: [MaterialComponents]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/pages/app-description/app-description.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/app-description/app-description.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  padding: 0 24px;\n}\n\n.app-questions {\n  justify-content: space-evenly;\n  width: 100%;\n  background-color: #4196cc;\n  min-height: 200px;\n  vertical-align: center;\n  color: white;\n}\n\n.text1 {\n  font-size: 150%;\n  align-self: center;\n}\n\n.item {\n  width: 100%;\n}\n\n.item > img {\n  width: 20%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n}\n\n.app-answers {\n  justify-content: space-between;\n}\n\n.text {\n  text-align: justify;\n  text-justify: auto;\n  word-break: break-all;\n  text-align: center;\n}\n\n.title {\n  font-size: 500%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXBwLWRlc2NyaXB0aW9uL0M6XFxVc2Vyc1xcTmVtYW5qYVxcRG9jdW1lbnRzXFxHaXQgUHJvamVjdHNcXGFuZ3VsYXItcHJvamVjdHNcXHRyYWluLW1lLW9ubGluZS9zcmNcXGFwcFxccGFnZXNcXGFwcC1kZXNjcmlwdGlvblxcYXBwLWRlc2NyaXB0aW9uLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9hcHAtZGVzY3JpcHRpb24vYXBwLWRlc2NyaXB0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBQTtBQ0NGOztBRENBO0VBQ0UsNkJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtBQ0VGOztBREFBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDR0Y7O0FEQUE7RUFDRSxXQUFBO0FDR0Y7O0FEREE7RUFDRSxVQUFBO0VBQ0EseUJBQUE7S0FBQSxzQkFBQTtBQ0lGOztBREZBO0VBQ0UsOEJBQUE7QUNLRjs7QURIQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0FDTUY7O0FESkE7RUFDRSxlQUFBO0FDT0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9hcHAtZGVzY3JpcHRpb24vYXBwLWRlc2NyaXB0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XHJcbiAgcGFkZGluZzogMCAyNHB4O1xyXG59XHJcbi5hcHAtcXVlc3Rpb25zIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNjUsIDE1MCwgMjA0KTtcclxuICBtaW4taGVpZ2h0OiAyMDBweDtcclxuICB2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4udGV4dDEge1xyXG4gIGZvbnQtc2l6ZTogMTUwJTtcclxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5pdGVtIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4uaXRlbSA+IGltZyB7XHJcbiAgd2lkdGg6IDIwJTtcclxuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xyXG59XHJcbi5hcHAtYW5zd2VycyB7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi50ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gIHRleHQtanVzdGlmeTogYXV0bztcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi50aXRsZSB7XHJcbiAgZm9udC1zaXplOiA1MDAlO1xyXG59XHJcbiIsIi5jb250YWluZXIge1xuICBwYWRkaW5nOiAwIDI0cHg7XG59XG5cbi5hcHAtcXVlc3Rpb25zIHtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDE5NmNjO1xuICBtaW4taGVpZ2h0OiAyMDBweDtcbiAgdmVydGljYWwtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4udGV4dDEge1xuICBmb250LXNpemU6IDE1MCU7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLml0ZW0ge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLml0ZW0gPiBpbWcge1xuICB3aWR0aDogMjAlO1xuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xufVxuXG4uYXBwLWFuc3dlcnMge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5cbi50ZXh0IHtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgdGV4dC1qdXN0aWZ5OiBhdXRvO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnRpdGxlIHtcbiAgZm9udC1zaXplOiA1MDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/app-description/app-description.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/app-description/app-description.component.ts ***!
  \********************************************************************/
/*! exports provided: AppDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppDescriptionComponent", function() { return AppDescriptionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppDescriptionComponent = /** @class */ (function () {
    function AppDescriptionComponent() {
    }
    AppDescriptionComponent.prototype.ngOnInit = function () {
    };
    AppDescriptionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-app-description',
            template: __webpack_require__(/*! raw-loader!./app-description.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/app-description/app-description.component.html"),
            styles: [__webpack_require__(/*! ./app-description.component.scss */ "./src/app/pages/app-description/app-description.component.scss")]
        })
    ], AppDescriptionComponent);
    return AppDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/pages/first-page/first-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/first-page/first-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@-webkit-keyframes pulse {\n  25% {\n    transform: scale(1);\n  }\n  50% {\n    transform: scale(1.03);\n  }\n}\n@keyframes pulse {\n  25% {\n    transform: scale(1);\n  }\n  50% {\n    transform: scale(1.03);\n  }\n}\n.pulse-button {\n  -webkit-animation: pulse 1s ease-in-out infinite;\n  animation: pulse 1s ease-in-out infinite;\n}\n.pulse-button:hover {\n  -webkit-animation: none;\n  animation: none;\n}\n.first-page {\n  background-color: white;\n  color: black;\n}\n.page-title {\n  font-weight: bolder;\n  font-size: 50px;\n  text-align: center;\n}\n.text-below-title {\n  font-size: 25px;\n  text-align: center;\n}\niframe {\n  width: 60%;\n  height: 100%;\n  min-height: 600px;\n  align-self: center;\n}\nform {\n  width: 60%;\n}\nmat-form-field, button {\n  width: 90%;\n  height: 60px;\n}\nbutton {\n  background-color: #ee506f;\n  color: white;\n  width: 70%;\n  font-size: 110%;\n}\n.text-below-form {\n  color: white;\n  background-color: #3F9BBF;\n  width: 60%;\n  font-size: 30px;\n  padding: 6px 0;\n  border-radius: 5px;\n}\n@media all and (max-width: 900px) {\n  .page-title {\n    font-size: 25px;\n  }\n\n  .text-below-title {\n    font-size: 15px;\n    text-align: left;\n  }\n\n  iframe {\n    width: 100%;\n    min-height: 200px;\n  }\n\n  form {\n    width: 100%;\n  }\n\n  .text-below-form {\n    font-size: 20px;\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZmlyc3QtcGFnZS9DOlxcVXNlcnNcXE5lbWFuamFcXERvY3VtZW50c1xcR2l0IFByb2plY3RzXFxhbmd1bGFyLXByb2plY3RzXFx0cmFpbi1tZS1vbmxpbmUvc3JjXFxhcHBcXHBhZ2VzXFxmaXJzdC1wYWdlXFxmaXJzdC1wYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9maXJzdC1wYWdlL2ZpcnN0LXBhZ2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBa0RBO0VBQ0U7SUFDRSxtQkFBQTtFQ2pERjtFRG1EQTtJQUNFLHNCQUFBO0VDakRGO0FBQ0Y7QUQyQ0E7RUFDRTtJQUNFLG1CQUFBO0VDakRGO0VEbURBO0lBQ0Usc0JBQUE7RUNqREY7QUFDRjtBRG1EQTtFQUNFLGdEQUFBO0VBR0Esd0NBQUE7QUNqREY7QURtREE7RUFDRSx1QkFBQTtFQUdBLGVBQUE7QUNoREY7QURvRkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUNqRkY7QURtRkE7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ2hGRjtBRG1GQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ2hGRjtBRGtGQTtFQUNFLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQy9FRjtBRGlGQTtFQUNFLFVBQUE7QUM5RUY7QURnRkE7RUFDRSxVQUFBO0VBQ0EsWUFBQTtBQzdFRjtBRGdGQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FDN0VGO0FEK0VBO0VBQ0UsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUM1RUY7QUQ4RUE7RUFDRTtJQUNFLGVBQUE7RUMzRUY7O0VENkVBO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0VDMUVGOztFRDRFQTtJQUNFLFdBQUE7SUFDQSxpQkFBQTtFQ3pFRjs7RUQyRUE7SUFDRSxXQUFBO0VDeEVGOztFRDBFQTtJQUNFLGVBQUE7SUFDQSxXQUFBO0VDdkVGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9maXJzdC1wYWdlL2ZpcnN0LXBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAuZmlyc3QtcGFnZSB7XHJcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogIzQwOGViZjtcclxuLy8gfVxyXG5cclxuLy8gaWZyYW1lIHtcclxuLy8gICB3aWR0aDogMTAwJTtcclxuLy8gICBoZWlnaHQ6IDEwMCU7XHJcbi8vICAgbWluLWhlaWdodDogNDAwcHg7XHJcbi8vIH1cclxuXHJcbi8vIC5tYXQtZGlzcGxheS0xLFxyXG4vLyAubWF0LWRpc3BsYXktMixcclxuLy8gLm1hdC1kaXNwbGF5LTMge1xyXG4vLyAgIG1hcmdpbi1ib3R0b206IDEycHg7XHJcbi8vIH1cclxuXHJcblxyXG5cclxuLy8gLndhcm4ge1xyXG4vLyAgIGNvbG9yOiAjZWU1MDZmO1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbi8vICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgIGxpbmUtaGVpZ2h0OjJ2dztcclxuLy8gfVxyXG4vLyBtYXQtZm9ybS1maWVsZCxcclxuLy8gYnV0dG9uIHtcclxuLy8gICB3aWR0aDogNjAlO1xyXG4vLyAgIGhlaWdodDogNzBweDtcclxuLy8gICBjb2xvcjogYmxhY2s7XHJcbi8vICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyB9XHJcbi8vIGJ1dHRvbiB7XHJcbi8vICAgZm9udC1zaXplOiAxMTAlO1xyXG4vLyAgIGJhY2tncm91bmQtY29sb3I6ICNlZTUwNmY7XHJcbi8vICAgY29sb3I6IHdoaXRlO1xyXG4vLyB9XHJcblxyXG4vLyBmb3JtIHtcclxuLy8gICB3aWR0aDogMTAwJTtcclxuLy8gICBtYXJnaW4tbGVmdDogNTVweDtcclxuLy8gfVxyXG4vLyBjb3VudGRvd24ge1xyXG4vLyAgIGxldHRlci1zcGFjaW5nOiAycHg7XHJcbi8vICAgY29sb3I6IHdoaXRlO1xyXG4vLyB9XHJcbi8vIG1hdC1pY29uIHtcclxuLy8gICBwYWRkaW5nLXJpZ2h0OiAzMnB4O1xyXG4vLyB9XHJcblxyXG5Aa2V5ZnJhbWVzIHB1bHNlIHtcclxuICAyNSUge1xyXG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICB9XHJcbiAgNTAlIHtcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4wMyk7XHJcbiAgfVxyXG59XHJcbi5wdWxzZS1idXR0b24ge1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBwdWxzZSAxcyBlYXNlLWluLW91dCBpbmZpbml0ZTtcclxuICAtbW96LWFuaW1hdGlvbjogcHVsc2UgMXMgZWFzZS1pbi1vdXQgaW5maW5pdGU7XHJcbiAgLW1zLWFuaW1hdGlvbjogcHVsc2UgMXMgZWFzZS1pbi1vdXQgaW5maW5pdGU7XHJcbiAgYW5pbWF0aW9uOiBwdWxzZSAxcyBlYXNlLWluLW91dCBpbmZpbml0ZTtcclxufVxyXG4ucHVsc2UtYnV0dG9uOmhvdmVyIHtcclxuICAtd2Via2l0LWFuaW1hdGlvbjogbm9uZTtcclxuICAtbW96LWFuaW1hdGlvbjogbm9uZTtcclxuICAtbXMtYW5pbWF0aW9uOiBub25lO1xyXG4gIGFuaW1hdGlvbjogbm9uZTtcclxufVxyXG5cclxuLy8gQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogMTI4MHB4KSB7XHJcbi8vICAgbWF0LWZvcm0tZmllbGQsXHJcbi8vICAgYnV0dG9uIHtcclxuLy8gICAgIHdpZHRoOiA5MCU7XHJcbi8vICAgICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XHJcbi8vICAgICBmb250LXNpemU6IDE1MCU7XHJcbi8vICAgfVxyXG4vLyAgIC5tYXQtZGlzcGxheS0xLFxyXG4vLyAgIC5tYXQtZGlzcGxheS0yIHtcclxuLy8gICAgIGZvbnQtc2l6ZTogMTUwJSAhaW1wb3J0YW50O1xyXG4vLyAgICAgbWFyZ2luLWJvdHRvbTogMTJweDtcclxuLy8gICB9XHJcbi8vICAgLm1hdC1kaXNwbGF5LTMge1xyXG4vLyAgICAgZm9udC1zaXplOiA4dncgIWltcG9ydGFudDtcclxuLy8gICAgIG1hcmdpbi1ib3R0b206IDEycHg7XHJcbi8vICAgICBtaW4td2lkdGg6IDEwMCU7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICBsaW5lLWhlaWdodDogOHZ3O1xyXG4vLyAgIH1cclxuLy8gICAud2FybiB7XHJcbi8vICAgICBsaW5lLWhlaWdodDogMjVweDtcclxuLy8gICB9XHJcbi8vICAgZm9ybSB7XHJcbi8vICAgICBtYXJnaW46IDA7XHJcbi8vICAgfVxyXG4gIFxyXG4vLyB9XHJcbi8vIEBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDQyMHB4KSB7XHJcbi8vICAgLnRleHQtbGlzdCB7XHJcbi8vICAgICBsaW5lLWhlaWdodDogN3Z3ICFpbXBvcnRhbnQ7XHJcbi8vICAgfVxyXG5cclxuLy8gfVxyXG4uZmlyc3QtcGFnZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi5wYWdlLXRpdGxlIHtcclxuICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi50ZXh0LWJlbG93LXRpdGxlIHtcclxuICBmb250LXNpemU6MjVweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuaWZyYW1lIHtcclxuICB3aWR0aDogNjAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBtaW4taGVpZ2h0OiA2MDBweDtcclxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbn1cclxuZm9ybSB7XHJcbiAgd2lkdGg6IDYwJTtcclxufVxyXG5tYXQtZm9ybS1maWVsZCwgYnV0dG9uIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIGhlaWdodDogNjBweDtcclxufVxyXG5cclxuYnV0dG9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWU1MDZmO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB3aWR0aDogNzAlO1xyXG4gIGZvbnQtc2l6ZTogMTEwJTtcclxufVxyXG4udGV4dC1iZWxvdy1mb3JtIHtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjojM0Y5QkJGO1xyXG4gIHdpZHRoOiA2MCU7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIHBhZGRpbmc6IDZweCAwO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOjkwMHB4KSB7XHJcbiAgLnBhZ2UtdGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gIH1cclxuICAudGV4dC1iZWxvdy10aXRsZSB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIH1cclxuICBpZnJhbWUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtaW4taGVpZ2h0OiAyMDBweDtcclxuICB9XHJcbiAgZm9ybSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgLnRleHQtYmVsb3ctZm9ybSB7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuICAiLCJAa2V5ZnJhbWVzIHB1bHNlIHtcbiAgMjUlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xuICB9XG4gIDUwJSB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjAzKTtcbiAgfVxufVxuLnB1bHNlLWJ1dHRvbiB7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBwdWxzZSAxcyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbiAgLW1vei1hbmltYXRpb246IHB1bHNlIDFzIGVhc2UtaW4tb3V0IGluZmluaXRlO1xuICAtbXMtYW5pbWF0aW9uOiBwdWxzZSAxcyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbiAgYW5pbWF0aW9uOiBwdWxzZSAxcyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbn1cblxuLnB1bHNlLWJ1dHRvbjpob3ZlciB7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBub25lO1xuICAtbW96LWFuaW1hdGlvbjogbm9uZTtcbiAgLW1zLWFuaW1hdGlvbjogbm9uZTtcbiAgYW5pbWF0aW9uOiBub25lO1xufVxuXG4uZmlyc3QtcGFnZSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5wYWdlLXRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1zaXplOiA1MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50ZXh0LWJlbG93LXRpdGxlIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmlmcmFtZSB7XG4gIHdpZHRoOiA2MCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbWluLWhlaWdodDogNjAwcHg7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuZm9ybSB7XG4gIHdpZHRoOiA2MCU7XG59XG5cbm1hdC1mb3JtLWZpZWxkLCBidXR0b24ge1xuICB3aWR0aDogOTAlO1xuICBoZWlnaHQ6IDYwcHg7XG59XG5cbmJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZTUwNmY7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDcwJTtcbiAgZm9udC1zaXplOiAxMTAlO1xufVxuXG4udGV4dC1iZWxvdy1mb3JtIHtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM0Y5QkJGO1xuICB3aWR0aDogNjAlO1xuICBmb250LXNpemU6IDMwcHg7XG4gIHBhZGRpbmc6IDZweCAwO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDkwMHB4KSB7XG4gIC5wYWdlLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gIH1cblxuICAudGV4dC1iZWxvdy10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cblxuICBpZnJhbWUge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICB9XG5cbiAgZm9ybSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cblxuICAudGV4dC1iZWxvdy1mb3JtIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/first-page/first-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/first-page/first-page.component.ts ***!
  \**********************************************************/
/*! exports provided: FirstPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirstPageComponent", function() { return FirstPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _new_user_snackbar_new_user_snackbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../new-user-snackbar/new-user-snackbar.component */ "./src/app/pages/new-user-snackbar/new-user-snackbar.component.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");






var FirstPageComponent = /** @class */ (function () {
    function FirstPageComponent(sanitizer, snackBar) {
        this.sanitizer = sanitizer;
        this.snackBar = snackBar;
        this.videoURL = 'https://www.youtube.com/embed/PzkiNG-DS18';
        this.users = [
            {
                name: 'Petar',
                city: 'Beograd',
                gender: 'M',
                image: '../../../assets/images/hanibal.jpg'
            }
        ];
        this.safeURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.videoURL);
    }
    FirstPageComponent.prototype.openSnackBar = function () {
        this.snackBar.openFromComponent(_new_user_snackbar_new_user_snackbar_component__WEBPACK_IMPORTED_MODULE_4__["NewUserSnackbarComponent"], {
            duration: 5000,
        });
    };
    FirstPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["interval"])(20000).subscribe(function (x) { return _this.openSnackBar(); });
    };
    FirstPageComponent.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"] }
    ]; };
    FirstPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-first-page',
            template: __webpack_require__(/*! raw-loader!./first-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/first-page/first-page.component.html"),
            styles: [__webpack_require__(/*! ./first-page.component.scss */ "./src/app/pages/first-page/first-page.component.scss")]
        })
    ], FirstPageComponent);
    return FirstPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/fourth-page/fourth-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/fourth-page/fourth-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fourth-page {\n  background-color: #80BEB9;\n  color: black;\n}\n\nimg {\n  width: 100%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n  margin-left: -12px;\n}\n\n.text-title {\n  font-size: 50px;\n  font-weight: bolder;\n  text-align: center;\n}\n\n.text-below-title {\n  width: 90%;\n  font-size: 25px;\n}\n\n@media all and (max-width: 900px) {\n  .text-title {\n    font-size: 25px;\n  }\n\n  .text-below-title {\n    font-size: 15px;\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZm91cnRoLXBhZ2UvQzpcXFVzZXJzXFxOZW1hbmphXFxEb2N1bWVudHNcXEdpdCBQcm9qZWN0c1xcYW5ndWxhci1wcm9qZWN0c1xcdHJhaW4tbWUtb25saW5lL3NyY1xcYXBwXFxwYWdlc1xcZm91cnRoLXBhZ2VcXGZvdXJ0aC1wYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9mb3VydGgtcGFnZS9mb3VydGgtcGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtBQ0NKOztBRENFO0VBQ0UsV0FBQTtFQUNBLHlCQUFBO0tBQUEsc0JBQUE7RUFDQSxrQkFBQTtBQ0VKOztBREFFO0VBQ0UsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNHSjs7QURERTtFQUNFLFVBQUE7RUFDQSxlQUFBO0FDSUo7O0FERkU7RUFDRTtJQUNFLGVBQUE7RUNLSjs7RURIRTtJQUNFLGVBQUE7SUFDQSxXQUFBO0VDTUo7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2ZvdXJ0aC1wYWdlL2ZvdXJ0aC1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvdXJ0aC1wYWdlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM4MEJFQjk7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgfVxyXG4gIGltZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XHJcbiAgICBtYXJnaW4tbGVmdDogLTEycHg7XHJcbiAgfVxyXG4gIC50ZXh0LXRpdGxlIHtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC50ZXh0LWJlbG93LXRpdGxlIHtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgfVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6OTAwcHgpIHtcclxuICAgIC50ZXh0LXRpdGxlIHtcclxuICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgfVxyXG4gICAgLnRleHQtYmVsb3ctdGl0bGUge1xyXG4gICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gIH0iLCIuZm91cnRoLXBhZ2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjODBCRUI5O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbmltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xuICBtYXJnaW4tbGVmdDogLTEycHg7XG59XG5cbi50ZXh0LXRpdGxlIHtcbiAgZm9udC1zaXplOiA1MHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50ZXh0LWJlbG93LXRpdGxlIHtcbiAgd2lkdGg6IDkwJTtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA5MDBweCkge1xuICAudGV4dC10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICB9XG5cbiAgLnRleHQtYmVsb3ctdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/fourth-page/fourth-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/fourth-page/fourth-page.component.ts ***!
  \************************************************************/
/*! exports provided: FourthPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FourthPageComponent", function() { return FourthPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FourthPageComponent = /** @class */ (function () {
    function FourthPageComponent() {
    }
    FourthPageComponent.prototype.ngOnInit = function () {
    };
    FourthPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fourth-page',
            template: __webpack_require__(/*! raw-loader!./fourth-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/fourth-page/fourth-page.component.html"),
            styles: [__webpack_require__(/*! ./fourth-page.component.scss */ "./src/app/pages/fourth-page/fourth-page.component.scss")]
        })
    ], FourthPageComponent);
    return FourthPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/header/header.component.scss":
/*!****************************************************!*\
  !*** ./src/app/pages/header/header.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header {\n  width: 100%;\n  padding: 12px 48px;\n  background-color: #0d0d0d;\n}\n\n.header-logo-container {\n  margin-left: -29px;\n}\n\n.header-logo {\n  width: 150px;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n}\n\nbutton {\n  background-color: #0d0d0d;\n  color: white;\n  border: 1px solid white;\n  font-size: 100%;\n  width: 100%;\n  height: 100%;\n}\n\n@media all and (max-width: 420px) {\n  .header {\n    padding: 6px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGVhZGVyL0M6XFxVc2Vyc1xcTmVtYW5qYVxcRG9jdW1lbnRzXFxHaXQgUHJvamVjdHNcXGFuZ3VsYXItcHJvamVjdHNcXHRyYWluLW1lLW9ubGluZS9zcmNcXGFwcFxccGFnZXNcXGhlYWRlclxcaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUNDRjs7QURFQTtFQUNFLGtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0QseUJBQUE7S0FBQSxzQkFBQTtBQ0NEOztBREdBO0VBQ0UseUJBQUE7RUFDRCxZQUFBO0VBQ0EsdUJBQUE7RUFDQyxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNBRjs7QURHQTtFQUNFO0lBQ0UsWUFBQTtFQ0FGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlciB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZzogMTJweCA0OHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwZDBkMGQ7XHJcbn1cclxuXHJcbi5oZWFkZXItbG9nby1jb250YWluZXIge1xyXG4gIG1hcmdpbi1sZWZ0OiAtMjlweDtcclxufVxyXG5cclxuLmhlYWRlci1sb2dvIHtcclxuICB3aWR0aDogMTUwcHg7XHJcbiBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xyXG5cclxufVxyXG5cclxuYnV0dG9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMGQwZDBkO1xyXG4gY29sb3I6IHdoaXRlO1xyXG4gYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XHJcbiAgZm9udC1zaXplOiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNDIwcHgpIHtcclxuICAuaGVhZGVyIHtcclxuICAgIHBhZGRpbmc6IDZweDtcclxuICB9XHJcbn0iLCIuaGVhZGVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDEycHggNDhweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzBkMGQwZDtcbn1cblxuLmhlYWRlci1sb2dvLWNvbnRhaW5lciB7XG4gIG1hcmdpbi1sZWZ0OiAtMjlweDtcbn1cblxuLmhlYWRlci1sb2dvIHtcbiAgd2lkdGg6IDE1MHB4O1xuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xufVxuXG5idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMGQwZDBkO1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xuICBmb250LXNpemU6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDQyMHB4KSB7XG4gIC5oZWFkZXIge1xuICAgIHBhZGRpbmc6IDZweDtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/header/header.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/header/header.component.ts ***!
  \**************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/pages/header/header.component.scss")]
        })
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/pages/new-user-snackbar/new-user-snackbar.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/pages/new-user-snackbar/new-user-snackbar.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "img {\n  width: 70px;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n  border-radius: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbmV3LXVzZXItc25hY2tiYXIvQzpcXFVzZXJzXFxOZW1hbmphXFxEb2N1bWVudHNcXEdpdCBQcm9qZWN0c1xcYW5ndWxhci1wcm9qZWN0c1xcdHJhaW4tbWUtb25saW5lL3NyY1xcYXBwXFxwYWdlc1xcbmV3LXVzZXItc25hY2tiYXJcXG5ldy11c2VyLXNuYWNrYmFyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9uZXctdXNlci1zbmFja2Jhci9uZXctdXNlci1zbmFja2Jhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLFdBQUE7RUFDQSx5QkFBQTtLQUFBLHNCQUFBO0VBQ0EsbUJBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL25ldy11c2VyLXNuYWNrYmFyL25ldy11c2VyLXNuYWNrYmFyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltZyB7XHJcbiAgICB3aWR0aDogNzBweDtcclxuICAgIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4iLCJpbWcge1xuICB3aWR0aDogNzBweDtcbiAgb2JqZWN0LWZpdDogc2NhbGUtZG93bjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/new-user-snackbar/new-user-snackbar.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/pages/new-user-snackbar/new-user-snackbar.component.ts ***!
  \************************************************************************/
/*! exports provided: NewUserSnackbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewUserSnackbarComponent", function() { return NewUserSnackbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NewUserSnackbarComponent = /** @class */ (function () {
    function NewUserSnackbarComponent() {
    }
    NewUserSnackbarComponent.prototype.ngOnInit = function () {
    };
    NewUserSnackbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-user-snackbar',
            template: __webpack_require__(/*! raw-loader!./new-user-snackbar.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/new-user-snackbar/new-user-snackbar.component.html"),
            styles: [__webpack_require__(/*! ./new-user-snackbar.component.scss */ "./src/app/pages/new-user-snackbar/new-user-snackbar.component.scss")]
        })
    ], NewUserSnackbarComponent);
    return NewUserSnackbarComponent;
}());



/***/ }),

/***/ "./src/app/pages/prices/prices.component.scss":
/*!****************************************************!*\
  !*** ./src/app/pages/prices/prices.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  padding: 0 24px;\n}\n\n.price-title > div {\n  text-align: center;\n  font-size: 150%;\n}\n\n.price-title {\n  background-color: #4196cc;\n  min-height: 200px;\n  color: white;\n}\n\n.price-item {\n  text-align: center;\n  background-color: #4196cc;\n  padding: 24px 12px;\n  min-width: 270px;\n  min-height: 370px;\n  color: white;\n}\n\n.price-item > div > span {\n  font-size: 500%;\n}\n\nmat-divider {\n  border-top-width: 5px;\n  margin-top: 10px;\n  border-color: white;\n}\n\n.euro-saver {\n  font-size: 90% !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJpY2VzL0M6XFxVc2Vyc1xcTmVtYW5qYVxcRG9jdW1lbnRzXFxHaXQgUHJvamVjdHNcXGFuZ3VsYXItcHJvamVjdHNcXHRyYWluLW1lLW9ubGluZS9zcmNcXGFwcFxccGFnZXNcXHByaWNlc1xccHJpY2VzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9wcmljZXMvcHJpY2VzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBQTtBQ0NGOztBREVBO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0FDQ0Y7O0FEQ0E7RUFDRSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtBQ0VGOztBREFBO0VBQ0Usa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNHRjs7QUREQTtFQUNFLGVBQUE7QUNJRjs7QURGQTtFQUNFLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0tGOztBREhBO0VBQ0UseUJBQUE7QUNNRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ByaWNlcy9wcmljZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyIHtcclxuICBwYWRkaW5nOiAwIDI0cHg7XHJcbn1cclxuXHJcbi5wcmljZS10aXRsZSA+IGRpdiB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMTUwJTtcclxufVxyXG4ucHJpY2UtdGl0bGUge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYig2NSwgMTUwLCAyMDQpO1xyXG4gIG1pbi1oZWlnaHQ6IDIwMHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4ucHJpY2UtaXRlbSB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYig2NSwgMTUwLCAyMDQpO1xyXG4gIHBhZGRpbmc6IDI0cHggMTJweDtcclxuICBtaW4td2lkdGg6IDI3MHB4O1xyXG4gIG1pbi1oZWlnaHQ6IDM3MHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4ucHJpY2UtaXRlbSA+IGRpdiA+IHNwYW4ge1xyXG4gIGZvbnQtc2l6ZTogNTAwJTtcclxufVxyXG5tYXQtZGl2aWRlciB7XHJcbiAgYm9yZGVyLXRvcC13aWR0aDogNXB4O1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxufVxyXG4uZXVyby1zYXZlciB7XHJcbiAgZm9udC1zaXplOiA5MCUgIWltcG9ydGFudDtcclxufVxyXG4iLCIuY29udGFpbmVyIHtcbiAgcGFkZGluZzogMCAyNHB4O1xufVxuXG4ucHJpY2UtdGl0bGUgPiBkaXYge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTUwJTtcbn1cblxuLnByaWNlLXRpdGxlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQxOTZjYztcbiAgbWluLWhlaWdodDogMjAwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnByaWNlLWl0ZW0ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0MTk2Y2M7XG4gIHBhZGRpbmc6IDI0cHggMTJweDtcbiAgbWluLXdpZHRoOiAyNzBweDtcbiAgbWluLWhlaWdodDogMzcwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnByaWNlLWl0ZW0gPiBkaXYgPiBzcGFuIHtcbiAgZm9udC1zaXplOiA1MDAlO1xufVxuXG5tYXQtZGl2aWRlciB7XG4gIGJvcmRlci10b3Atd2lkdGg6IDVweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmV1cm8tc2F2ZXIge1xuICBmb250LXNpemU6IDkwJSAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/prices/prices.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/prices/prices.component.ts ***!
  \**************************************************/
/*! exports provided: PricesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PricesComponent", function() { return PricesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PricesComponent = /** @class */ (function () {
    function PricesComponent() {
    }
    PricesComponent.prototype.ngOnInit = function () {
    };
    PricesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-prices',
            template: __webpack_require__(/*! raw-loader!./prices.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/prices/prices.component.html"),
            styles: [__webpack_require__(/*! ./prices.component.scss */ "./src/app/pages/prices/prices.component.scss")]
        })
    ], PricesComponent);
    return PricesComponent;
}());



/***/ }),

/***/ "./src/app/pages/second-page/second-page.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/second-page/second-page.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".second-page {\n  background-color: #408EBF;\n  color: white;\n}\n\nimg {\n  width: 100%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n  margin-left: -12px;\n}\n\n.text-title {\n  font-size: 50px;\n  font-weight: bolder;\n  text-align: center;\n}\n\n.text-below-title {\n  width: 90%;\n  font-size: 25px;\n}\n\n@media all and (max-width: 900px) {\n  .text-title {\n    font-size: 25px;\n  }\n\n  .text-below-title {\n    font-size: 15px;\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2Vjb25kLXBhZ2UvQzpcXFVzZXJzXFxOZW1hbmphXFxEb2N1bWVudHNcXEdpdCBQcm9qZWN0c1xcYW5ndWxhci1wcm9qZWN0c1xcdHJhaW4tbWUtb25saW5lL3NyY1xcYXBwXFxwYWdlc1xcc2Vjb25kLXBhZ2VcXHNlY29uZC1wYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9zZWNvbmQtcGFnZS9zZWNvbmQtcGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUErQ0E7RUFDRSx5QkFBQTtFQUNBLFlBQUE7QUM5Q0Y7O0FEZ0RBO0VBQ0UsV0FBQTtFQUNBLHlCQUFBO0tBQUEsc0JBQUE7RUFDQSxrQkFBQTtBQzdDRjs7QUQrQ0E7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQzVDRjs7QUQ4Q0E7RUFDRSxVQUFBO0VBQ0EsZUFBQTtBQzNDRjs7QUQ2Q0E7RUFDRTtJQUNFLGVBQUE7RUMxQ0Y7O0VENENBO0lBQ0UsZUFBQTtJQUNBLFdBQUE7RUN6Q0Y7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NlY29uZC1wYWdlL3NlY29uZC1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLnNlY29uZC1wYWdlLWltYWdlLXdyYXBwZXIge1xyXG4vLyAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi8uLi9hc3NldHMvaW1hZ2VzL3N0cm9uZ21hbi1maXJzdC1waWN0dXJlLnBuZycpO1xyXG4vLyAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4vLyAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuLy8gICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbi8vICAgICB3aWR0aDogOTB2dztcclxuLy8gICAgIGhlaWdodDogMTAwJTtcclxuLy8gICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZVgoLTEpO1xyXG4vLyAgICAgdHJhbnNmb3JtOiBzY2FsZVgoLTEpO1xyXG4vLyAgICAgei1pbmRleDogLTE7XHJcbi8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vICAgICBvcGFjaXR5OiAwLjM7XHJcbi8vICAgfVxyXG4vLyAgIC5tYXQtZGlzcGxheS0zIHtcclxuLy8gICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbi8vICAgICBmb250LXNpemU6IDV2aDtcclxuLy8gICB9XHJcbi8vICAgLm1hdC1kaXNwbGF5LTEge1xyXG4vLyAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuLy8gICAgIGZvbnQtc2l6ZTogM3ZoO1xyXG4vLyAgIH1cclxuLy8gICBidXR0b24ge1xyXG4vLyAgICAgd2lkdGg6IDYwJTtcclxuLy8gICAgIGJhY2tncm91bmQtY29sb3I6ICNlZTUwNmY7XHJcbi8vICAgICBoZWlnaHQ6IDEwdmg7XHJcbi8vICAgICBjb2xvcjogd2hpdGU7XHJcbi8vICAgICBmb250LXNpemU6IDExMCU7XHJcbi8vICAgfVxyXG4vLyAgIC5zZWNvbmQtcGFnZS10ZXh0IHtcclxuLy8gICAgIHotaW5kZXg6IDEwMDtcclxuLy8gICB9XHJcbi8vICAgQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogMTI4MHB4KSB7XHJcbi8vICAgICAubWF0LWRpc3BsYXktMyB7XHJcbi8vICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gICAgIH1cclxuLy8gICAgIC5tYXQtZGlzcGxheS0xIHtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiA1dnc7ICAgICAgXHJcbi8vICAgICB9XHJcbi8vICAgfVxyXG5cclxuLy8gICBAbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA0MjBweCkge1xyXG4vLyAgICAgLm1hdC1kaXNwbGF5LTEge1xyXG4vLyAgICAgICBsaW5lLWhlaWdodDogN3Z3OyAgICAgIFxyXG4vLyAgICAgICB9XHJcblxyXG4vLyAgIH1cclxuXHJcbi5zZWNvbmQtcGFnZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQwOEVCRjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuaW1nIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTJweDtcclxufVxyXG4udGV4dC10aXRsZSB7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi50ZXh0LWJlbG93LXRpdGxlIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIGZvbnQtc2l6ZTogMjVweDtcclxufVxyXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOjkwMHB4KSB7XHJcbiAgLnRleHQtdGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gIH1cclxuICAudGV4dC1iZWxvdy10aXRsZSB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn0iLCIuc2Vjb25kLXBhZ2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDA4RUJGO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbmltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xuICBtYXJnaW4tbGVmdDogLTEycHg7XG59XG5cbi50ZXh0LXRpdGxlIHtcbiAgZm9udC1zaXplOiA1MHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50ZXh0LWJlbG93LXRpdGxlIHtcbiAgd2lkdGg6IDkwJTtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA5MDBweCkge1xuICAudGV4dC10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICB9XG5cbiAgLnRleHQtYmVsb3ctdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/second-page/second-page.component.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/second-page/second-page.component.ts ***!
  \************************************************************/
/*! exports provided: SecondPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecondPageComponent", function() { return SecondPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SecondPageComponent = /** @class */ (function () {
    function SecondPageComponent() {
    }
    SecondPageComponent.prototype.ngOnInit = function () {
    };
    SecondPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-second-page',
            template: __webpack_require__(/*! raw-loader!./second-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/second-page/second-page.component.html"),
            styles: [__webpack_require__(/*! ./second-page.component.scss */ "./src/app/pages/second-page/second-page.component.scss")]
        })
    ], SecondPageComponent);
    return SecondPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/strongman/strongman.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/strongman/strongman.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  padding: 0 24px;\n}\n\n.strongman-question {\n  background-color: #4196cc;\n  min-height: 200px;\n  color: white;\n}\n\n.strongman-question > div {\n  margin: 0;\n}\n\n.image {\n  height: 500px;\n  width: 500px;\n}\n\ndiv > img {\n  width: 100%;\n  border-radius: 50%;\n  height: 100%;\n}\n\n.text {\n  font-weight: lighter;\n  font-size: 150%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc3Ryb25nbWFuL0M6XFxVc2Vyc1xcTmVtYW5qYVxcRG9jdW1lbnRzXFxHaXQgUHJvamVjdHNcXGFuZ3VsYXItcHJvamVjdHNcXHRyYWluLW1lLW9ubGluZS9zcmNcXGFwcFxccGFnZXNcXHN0cm9uZ21hblxcc3Ryb25nbWFuLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9zdHJvbmdtYW4vc3Ryb25nbWFuLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBQTtBQ0NGOztBRENBO0VBQ0UseUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNFRjs7QURBQTtFQUNFLFNBQUE7QUNHRjs7QUREQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDSUY7O0FERkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDS0Y7O0FERkE7RUFDRSxvQkFBQTtFQUNBLGVBQUE7QUNLRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3N0cm9uZ21hbi9zdHJvbmdtYW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyIHtcclxuICBwYWRkaW5nOiAwIDI0cHg7XHJcbn1cclxuLnN0cm9uZ21hbi1xdWVzdGlvbiB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDY1LCAxNTAsIDIwNCk7XHJcbiAgbWluLWhlaWdodDogMjAwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcbi5zdHJvbmdtYW4tcXVlc3Rpb24gPiBkaXYge1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG4uaW1hZ2Uge1xyXG4gIGhlaWdodDogNTAwcHg7XHJcbiAgd2lkdGg6IDUwMHB4O1xyXG59XHJcbmRpdiA+IGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLnRleHQge1xyXG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xyXG4gIGZvbnQtc2l6ZTogMTUwJTtcclxufVxyXG4iLCIuY29udGFpbmVyIHtcbiAgcGFkZGluZzogMCAyNHB4O1xufVxuXG4uc3Ryb25nbWFuLXF1ZXN0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQxOTZjYztcbiAgbWluLWhlaWdodDogMjAwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnN0cm9uZ21hbi1xdWVzdGlvbiA+IGRpdiB7XG4gIG1hcmdpbjogMDtcbn1cblxuLmltYWdlIHtcbiAgaGVpZ2h0OiA1MDBweDtcbiAgd2lkdGg6IDUwMHB4O1xufVxuXG5kaXYgPiBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi50ZXh0IHtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG4gIGZvbnQtc2l6ZTogMTUwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/strongman/strongman.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/strongman/strongman.component.ts ***!
  \********************************************************/
/*! exports provided: StrongmanComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StrongmanComponent", function() { return StrongmanComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var StrongmanComponent = /** @class */ (function () {
    function StrongmanComponent(http) {
        this.http = http;
        this.text = '';
        this.http
            .get('assets/text/test.txt', { responseType: 'text' })
            .subscribe(function (data) { return console.log(data); });
    }
    StrongmanComponent.prototype.ngOnInit = function () { };
    StrongmanComponent.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    StrongmanComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-strongman',
            template: __webpack_require__(/*! raw-loader!./strongman.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/strongman/strongman.component.html"),
            styles: [__webpack_require__(/*! ./strongman.component.scss */ "./src/app/pages/strongman/strongman.component.scss")]
        })
    ], StrongmanComponent);
    return StrongmanComponent;
}());



/***/ }),

/***/ "./src/app/pages/third-page/third-page.component.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/third-page/third-page.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".third-page {\n  background-color: #0d0d0d;\n  color: white;\n}\n\nimg {\n  width: 100%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n}\n\n.text-title {\n  font-size: 50px;\n  font-weight: bolder;\n  text-align: center;\n}\n\n.text-below-title {\n  width: 90%;\n  font-size: 25px;\n}\n\n@media all and (max-width: 900px) {\n  .text-title {\n    font-size: 25px;\n  }\n\n  .text-below-title {\n    font-size: 15px;\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGhpcmQtcGFnZS9DOlxcVXNlcnNcXE5lbWFuamFcXERvY3VtZW50c1xcR2l0IFByb2plY3RzXFxhbmd1bGFyLXByb2plY3RzXFx0cmFpbi1tZS1vbmxpbmUvc3JjXFxhcHBcXHBhZ2VzXFx0aGlyZC1wYWdlXFx0aGlyZC1wYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy90aGlyZC1wYWdlL3RoaXJkLXBhZ2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7QUNDSjs7QURDRTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtLQUFBLHNCQUFBO0FDRUo7O0FEQUU7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ0dKOztBRERFO0VBQ0UsVUFBQTtFQUNBLGVBQUE7QUNJSjs7QURGRTtFQUNFO0lBQ0UsZUFBQTtFQ0tKOztFREhFO0lBQ0UsZUFBQTtJQUNBLFdBQUE7RUNNSjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGhpcmQtcGFnZS90aGlyZC1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRoaXJkLXBhZ2Uge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzBkMGQwZDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgaW1nIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb2JqZWN0LWZpdDogc2NhbGUtZG93bjtcclxuICB9XHJcbiAgLnRleHQtdGl0bGUge1xyXG4gICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLnRleHQtYmVsb3ctdGl0bGUge1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICB9XHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDo5MDBweCkge1xyXG4gICAgLnRleHQtdGl0bGUge1xyXG4gICAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICB9XHJcbiAgICAudGV4dC1iZWxvdy10aXRsZSB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfSIsIi50aGlyZC1wYWdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzBkMGQwZDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG5pbWcge1xuICB3aWR0aDogMTAwJTtcbiAgb2JqZWN0LWZpdDogc2NhbGUtZG93bjtcbn1cblxuLnRleHQtdGl0bGUge1xuICBmb250LXNpemU6IDUwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnRleHQtYmVsb3ctdGl0bGUge1xuICB3aWR0aDogOTAlO1xuICBmb250LXNpemU6IDI1cHg7XG59XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDkwMHB4KSB7XG4gIC50ZXh0LXRpdGxlIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gIH1cblxuICAudGV4dC1iZWxvdy10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/third-page/third-page.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/third-page/third-page.component.ts ***!
  \**********************************************************/
/*! exports provided: ThirdPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThirdPageComponent", function() { return ThirdPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ThirdPageComponent = /** @class */ (function () {
    function ThirdPageComponent() {
    }
    ThirdPageComponent.prototype.ngOnInit = function () {
    };
    ThirdPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-third-page',
            template: __webpack_require__(/*! raw-loader!./third-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/third-page/third-page.component.html"),
            styles: [__webpack_require__(/*! ./third-page.component.scss */ "./src/app/pages/third-page/third-page.component.scss")]
        })
    ], ThirdPageComponent);
    return ThirdPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/trainers/trainers.component.scss":
/*!********************************************************!*\
  !*** ./src/app/pages/trainers/trainers.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  padding: 0 24px;\n}\n\n.trainer-question {\n  justify-content: center;\n  background-color: #4196cc;\n  min-height: 200px;\n  color: white;\n}\n\n.trainer-question > div {\n  font-size: 150%;\n  align-self: center;\n}\n\ndiv {\n  text-align: center;\n}\n\n.app-image {\n  width: 100%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n}\n\n.text {\n  text-align: left;\n  word-break: break-all;\n}\n\n.trainer-image {\n  width: 20%;\n  -o-object-fit: scale-down;\n     object-fit: scale-down;\n  border-radius: 50%;\n  z-index: 2;\n}\n\n.text-wrapper {\n  background-color: #4196cc;\n  color: white;\n  width: 100%;\n  padding-left: 100px;\n  margin-left: -80px;\n}\n\nbutton {\n  color: white;\n  background-color: #4196cc;\n  font-size: 150%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdHJhaW5lcnMvQzpcXFVzZXJzXFxOZW1hbmphXFxEb2N1bWVudHNcXEdpdCBQcm9qZWN0c1xcYW5ndWxhci1wcm9qZWN0c1xcdHJhaW4tbWUtb25saW5lL3NyY1xcYXBwXFxwYWdlc1xcdHJhaW5lcnNcXHRyYWluZXJzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy90cmFpbmVycy90cmFpbmVycy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7QUNDRjs7QURDQTtFQUNFLHVCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNFRjs7QURBQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ0dGOztBREFBO0VBQ0Usa0JBQUE7QUNHRjs7QUREQTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtLQUFBLHNCQUFBO0FDSUY7O0FEREE7RUFDRSxnQkFBQTtFQUNBLHFCQUFBO0FDSUY7O0FERkE7RUFDRSxVQUFBO0VBQ0EseUJBQUE7S0FBQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQ0tGOztBREhBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNNRjs7QURKQTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7QUNPRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RyYWluZXJzL3RyYWluZXJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XHJcbiAgcGFkZGluZzogMCAyNHB4O1xyXG59XHJcbi50cmFpbmVyLXF1ZXN0aW9uIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNjUsIDE1MCwgMjA0KTtcclxuICBtaW4taGVpZ2h0OiAyMDBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLnRyYWluZXItcXVlc3Rpb24gPiBkaXYge1xyXG4gIGZvbnQtc2l6ZTogMTUwJTtcclxuICBhbGlnbi1zZWxmOiBjZW50ZXI7XHJcbn1cclxuXHJcbmRpdiB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5hcHAtaW1hZ2Uge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XHJcbn1cclxuXHJcbi50ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxufVxyXG4udHJhaW5lci1pbWFnZSB7XHJcbiAgd2lkdGg6IDIwJTtcclxuICBvYmplY3QtZml0OiBzY2FsZS1kb3duO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICB6LWluZGV4OiAyO1xyXG59XHJcbi50ZXh0LXdyYXBwZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYig2NSwgMTUwLCAyMDQpO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBwYWRkaW5nLWxlZnQ6IDEwMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAtODBweDtcclxufVxyXG5idXR0b24ge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoNjUsIDE1MCwgMjA0KTtcclxuICBmb250LXNpemU6IDE1MCU7XHJcbn1cclxuIiwiLmNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDAgMjRweDtcbn1cblxuLnRyYWluZXItcXVlc3Rpb24ge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQxOTZjYztcbiAgbWluLWhlaWdodDogMjAwcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLnRyYWluZXItcXVlc3Rpb24gPiBkaXYge1xuICBmb250LXNpemU6IDE1MCU7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuZGl2IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYXBwLWltYWdlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XG59XG5cbi50ZXh0IHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xufVxuXG4udHJhaW5lci1pbWFnZSB7XG4gIHdpZHRoOiAyMCU7XG4gIG9iamVjdC1maXQ6IHNjYWxlLWRvd247XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgei1pbmRleDogMjtcbn1cblxuLnRleHQtd3JhcHBlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0MTk2Y2M7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctbGVmdDogMTAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAtODBweDtcbn1cblxuYnV0dG9uIHtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDE5NmNjO1xuICBmb250LXNpemU6IDE1MCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/trainers/trainers.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/trainers/trainers.component.ts ***!
  \******************************************************/
/*! exports provided: TrainersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrainersComponent", function() { return TrainersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TrainersComponent = /** @class */ (function () {
    function TrainersComponent() {
    }
    TrainersComponent.prototype.ngOnInit = function () {
    };
    TrainersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-trainers',
            template: __webpack_require__(/*! raw-loader!./trainers.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/trainers/trainers.component.html"),
            styles: [__webpack_require__(/*! ./trainers.component.scss */ "./src/app/pages/trainers/trainers.component.scss")]
        })
    ], TrainersComponent);
    return TrainersComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Nemanja\Documents\Git Projects\angular-projects\train-me-online\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map